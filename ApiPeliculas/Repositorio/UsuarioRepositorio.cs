using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Security.Cryptography;
using ApiPeliculas.Data;
using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using ApiPeliculas.Repositorio.IRepositorio;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace ApiPeliculas.Repositorio
{
    public class UsuarioRepositorio : IUsuarioRepository
    {


        private readonly ApplicationDbContext _dbContex;
        private string claveSecreta;
        public UsuarioRepositorio(ApplicationDbContext dbContext, IConfiguration config ){
            _dbContex = dbContext;;
            claveSecreta = config.GetValue<string>("ApiSettings:Secreta");
        }
     
        public Usuario getUsuario(int usuarioId)
        {
            return _dbContex.Usuario.FirstOrDefault(u => u.Id == usuarioId);
        }

        public ICollection<Usuario> GetUsuarios()
        {
            return _dbContex.Usuario.OrderBy(u => u.Nombre).ToList();
        }

        public bool IsUniqueUser(string usuario)
        {
            var usuarioDb = _dbContex.Usuario.FirstOrDefault(u => u.NombreUsuario == usuario);

            if (usuarioDb == null){
                return true;
            }

            return false;
        }
        public async Task<Usuario> Registro(UsuarioRegistroDto usuarioRegistroDto)
        {
            var passwordEncryptado = obtenerMd5(usuarioRegistroDto.password);

            Usuario usuario = new Usuario()
            {
                Nombre = usuarioRegistroDto.Nombre,
                Password = passwordEncryptado,
                NombreUsuario = usuarioRegistroDto.NombreUsuario,
                Role = usuarioRegistroDto.Role
            };

            _dbContex.Usuario.Add(usuario);
            await _dbContex.SaveChangesAsync();

            usuario.Password = passwordEncryptado;
            return usuario;

        }
        public async Task<UsuarioLoginRespuestaDto> Login(UsuarioLoginDto usuarioLoginDto)
        {
            var passwordEncryptado = obtenerMd5(usuarioLoginDto.password);

            var usuario = _dbContex.Usuario.FirstOrDefault (
            u => u.NombreUsuario.ToLower() == usuarioLoginDto.NombreUsuario
            && u.Password == passwordEncryptado
            );

            // validamos si el usuarios no existe con la combinacion de usuario y contraseña

            if (usuario == null)
            return  new UsuarioLoginRespuestaDto(){
                token ="",
                usuario = null
            };
            
            // aqui existe el usaurio entonces podemos procasar el login
            var manejadorToken = new JwtSecurityTokenHandler();
            var key =  Encoding.ASCII.GetBytes(claveSecreta);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.Name, usuario.NombreUsuario.ToString()),
                    new Claim(ClaimTypes.Role,usuario.Role)
                }),
                Expires = DateTime.Now.AddDays(7),
                SigningCredentials = new(new SymmetricSecurityKey(key), SecurityAlgorithms.RsaSha256Signature)
            };

            var token = manejadorToken.CreateToken(tokenDescriptor);

            UsuarioLoginRespuestaDto usuarioLoginRespuestaDto = new UsuarioLoginRespuestaDto(){
                token = manejadorToken.WriteToken(token),
                usuario = usuario
            };

            return usuarioLoginRespuestaDto;
        }

        public static string obtenerMd5 (string valor)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(valor);
            data = MD5.HashData(data);
            string resp = "";
            for (int i = 0; i < data.Length; i++)
                resp += data[i].ToString("x2").ToLower();

            return resp;
        }
    }
}