using ApiPeliculas.Data;
using ApiPeliculas.Models;
using ApiPeliculas.Repositorio.IRepositorio;

namespace ApiPeliculas.Repositorio;

public class CategoriaRepositorio : ICategoriaRepository
{

    private readonly ApplicationDbContext _dbContex;

    public CategoriaRepositorio(ApplicationDbContext dbContex)
    {
        _dbContex = dbContex;
    }
    
    public ICollection<Categoria> GetCategorias()
    {
        return _dbContex.Categoria.OrderBy(c => c.Nombre).ToList();
    }

    public Categoria getCategoria(int categoriaId)
    {
        return _dbContex.Categoria.FirstOrDefault(c => c.Id == categoriaId);
    }

    public bool ExisteCategoria(string nombre)
    {
        bool valor = _dbContex.Categoria.Any(c => c.Nombre.ToLower().Trim() == nombre.ToLower().Trim());
        return valor; 
    }

    public bool ExisteCategoria(int id)
    {
        return _dbContex.Categoria.Any(c => c.Id == id);
    }

    public bool CreateCategoria(Categoria categoria)
    {
        categoria.FechaCreacion = DateTime.Now;
        _dbContex.Update(categoria);
        return Guardar();
        
    }

    public bool ActualizarCategoria(Categoria categoria)
    {
        categoria.FechaCreacion = DateTime.Now;
        _dbContex.Update(categoria);
        return Guardar();
    }

    public bool BorrarCategoria(Categoria categoria)
    {
        _dbContex.Categoria.Remove(categoria);
        return Guardar();
    }

    public bool Guardar()
    {
        return _dbContex.SaveChanges() >= 0;
    }
}