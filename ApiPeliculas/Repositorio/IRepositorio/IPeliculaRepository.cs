using ApiPeliculas.Models;

namespace ApiPeliculas.Repositorio.IRepositorio;

public interface IPeliculaRepository
{
    ICollection<Pelicula> GetPeliculas();
    Pelicula getPelicula(int peliculaId);
    bool ExistePelicula(string nombre);
    bool ExistePelicula(int id);
    bool CreatePelicula(Pelicula pelicula);
    bool ActualizarPelicula(Pelicula pelicula);
    bool BorrarPelicula(Pelicula pelicula);

    // métodos para buscar película en categoria y buscar por el nombre
    ICollection<Pelicula> GetPeliculasEnCategoria(int idCategoria);
    ICollection<Pelicula> BuscarPelicula(string nombre);

    bool Guardar();
    
}