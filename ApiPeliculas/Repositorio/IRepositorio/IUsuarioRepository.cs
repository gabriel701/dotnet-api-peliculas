using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;

namespace ApiPeliculas.Repositorio.IRepositorio;

public interface IUsuarioRepository
{
    ICollection<Usuario> GetUsuarios();
    Usuario getUsuario(int usuarioId);
    bool IsUniqueUser(String usuario);
    Task<UsuarioLoginRespuestaDto> Login(UsuarioLoginDto usuarioLoginDto);
    Task<Usuario>  Registro(UsuarioRegistroDto usuarioRegistroDto);
}