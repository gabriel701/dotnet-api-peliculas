using ApiPeliculas.Models;

namespace ApiPeliculas.Repositorio.IRepositorio;

public interface ICategoriaRepository
{
    ICollection<Categoria> GetCategorias();
    Categoria getCategoria(int categoriaId);
    bool ExisteCategoria(string nombre);
    bool ExisteCategoria(int id);
    bool CreateCategoria(Categoria categoria);
    bool ActualizarCategoria(Categoria categoria);
    bool BorrarCategoria(Categoria categoria);
    bool Guardar();
    
}