using ApiPeliculas.Data;
using ApiPeliculas.Models;
using ApiPeliculas.Repositorio.IRepositorio;
using Microsoft.EntityFrameworkCore;

namespace ApiPeliculas.Repositorio;

public class PeliculaRepositorio : IPeliculaRepository
{

    private readonly ApplicationDbContext _dbContex;

    public PeliculaRepositorio(ApplicationDbContext dbContex)
    {
        _dbContex = dbContex;
    }
    
    public ICollection<Pelicula> GetPeliculas()
    {
        return _dbContex.Pelicula.OrderBy(c => c.Nombre).ToList();
    }

    public Pelicula getPelicula(int peliculaId)
    {
        return _dbContex.Pelicula.FirstOrDefault(c => c.Id == peliculaId);
    }

    public bool ExistePelicula(string nombre)
    {
        bool valor = _dbContex.Pelicula.Any(c => c.Nombre.ToLower().Trim() == nombre.ToLower().Trim());
        return valor; 
    }

    public bool ExistePelicula(int id)
    {
        return _dbContex.Pelicula.Any(c => c.Id == id);
    }

    public bool CreatePelicula(Pelicula pelicula)
    {
        pelicula.FechaCreacion = DateTime.Now;
        _dbContex.Update(pelicula);
        return Guardar();
        
    }

    public bool ActualizarPelicula(Pelicula pelicula)
    {
        pelicula.FechaCreacion = DateTime.Now;
        _dbContex.Update(pelicula);
        return Guardar();
    }

    public bool BorrarPelicula(Pelicula pelicula)
    {
        _dbContex.Pelicula.Remove(pelicula);
        return Guardar();
    }

    public bool Guardar()
    {
        return _dbContex.SaveChanges() >= 0;
    }

    public ICollection<Pelicula> GetPeliculasEnCategoria(int idCategoria)
    {
        return _dbContex.Pelicula.Include(ca => ca.Categoria).Where(ca => ca.Id == idCategoria).ToList();
    }

    public ICollection<Pelicula> BuscarPelicula(string nombre)
    {
        IQueryable<Pelicula> Query = _dbContex.Pelicula;

        if (!string.IsNullOrEmpty(nombre)){
            Query = Query.Where(e => e.Nombre.Contains(nombre) || e.Descripcion.Contains(nombre));
        }

        return Query.ToList();
    }
}