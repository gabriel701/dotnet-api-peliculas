using System.Runtime.InteropServices.ComTypes;
using ApiPeliculas.Models;
using ApiPeliculas.Repositorio.IRepositorio;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ApiPeliculas.Controllers;

[ApiController]
[Route("api/categorias")]
public class CategoriasController : ControllerBase
{
    private readonly ICategoriaRepository _ctRepo;

    private readonly IMapper _iMapper;

    public CategoriasController(ICategoriaRepository ctRepo, IMapper iMapper)
    {
        _ctRepo = ctRepo;
        _iMapper = iMapper;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public IActionResult getCategorias()
    {
        var ListeCategorias = _ctRepo.GetCategorias();
        var ListaCategoriasDto = new List<CategoriaDto>();


        foreach (var lista in ListeCategorias)
        {
            ListaCategoriasDto.Add(_iMapper.Map<CategoriaDto>(lista));
        }
        return Ok(ListaCategoriasDto);
    }

    [HttpGet(("CategoriaId:int"), Name = "getCategoria")]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult getCategoria(int categoriaId)
    {
        var itemCategoria = _ctRepo.getCategoria(categoriaId);

        if (itemCategoria == null)
        {
            return NotFound();
        }

        var itemCategoriaDto = _iMapper.Map<CategoriaDto>(itemCategoria);
        return Ok(itemCategoriaDto);
    }
    
    [HttpPost]
    [ProducesResponseType(201, Type = typeof(CategoriaDto))]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult CrearCategoria([FromBody] CrearCategoriaDto crearCategoriaDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        if (crearCategoriaDto == null)
        {
            return BadRequest(ModelState);
        }

        if (_ctRepo.ExisteCategoria(crearCategoriaDto.Nombre))
        {
            ModelState.AddModelError("","La categoria ya existe");
            return StatusCode(404, ModelState);
        }

        var categoria = _iMapper.Map<Categoria>(crearCategoriaDto);
        if (!_ctRepo.CreateCategoria(categoria))
        {
            ModelState.AddModelError("",$"Algo salió mal guardando el registro {categoria.Nombre}");
            return StatusCode(500, ModelState);
        }

        return CreatedAtRoute("GetCategoria", new { categoriaId = categoria.Id }, categoria);
    }
    
    [HttpPatch(("categoriaId:int"), Name = "ActualizarPatchCategoria")]
    [ProducesResponseType(201, Type = typeof(CategoriaDto))]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult ActualizarPatchCategoria(int categoriaId, [FromBody] CategoriaDto categoriaDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        if (categoriaDto == null || categoriaId!=categoriaDto.Id)
        {
            return BadRequest(ModelState);
        }

        var categoria = _iMapper.Map<Categoria>(categoriaDto);
        if (!_ctRepo.ActualizarCategoria(categoria))
        {
            ModelState.AddModelError("",$"Algo salió mal actualizando el registro {categoria.Nombre}");
            return StatusCode(500, ModelState);
        }

        return NoContent();
    }
    
    
    [HttpDelete(("categoriaId:int"), Name = "BorrarCategoria")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult BorrarCategoria(int categoriaId)
    {

        if (!_ctRepo.ExisteCategoria(categoriaId))
        {
            return NotFound();
        }

        var categoria = _ctRepo.getCategoria(categoriaId);
        
        if (!_ctRepo.BorrarCategoria(categoria))
        {
            ModelState.AddModelError("",$"Algo salió mal borrano el registro {categoria.Nombre}");
            return StatusCode(500, ModelState);
        }
        return NoContent();
    }
}