using System.Net;
using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using ApiPeliculas.Repositorio.IRepositorio;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ApiPeliculas.Controllers
{
    [ApiController]
    [Route("api/usuarios")]
    public class UsuariosController : ControllerBase
    {

        private readonly IUsuarioRepository _repo;
        protected RespuestaApi _respuestaApi;
        private readonly IMapper _iMapper;

        public UsuariosController(IUsuarioRepository uRepo, IMapper iMapper)
        {
            _repo = uRepo;
            _iMapper = iMapper;
            this._respuestaApi = new();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult getUsuarios()
        {
            var ListaUsuarios = _repo.GetUsuarios();
            var ListaUsuariosDto = new List<UsuarioDto>();


            foreach (var lista in ListaUsuarios)
            {
                ListaUsuariosDto.Add(_iMapper.Map<UsuarioDto>(lista));
            }
            return Ok(ListaUsuariosDto);
        }

        [HttpGet(("usuarioId:int"), Name = "getUsuario")]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult getUsuario(int usuarioId)
        {
            var itemUsuario= _repo.getUsuario(usuarioId);

            if (itemUsuario == null)
            {
                return NotFound();
            }

            var itemUsuarioDto = _iMapper.Map<UsuarioDto>(itemUsuario);
            return Ok(itemUsuarioDto);
        }        

        [HttpPost("registro")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> registro([FromBody] UsuarioRegistroDto usuarioRegistroDto)
        {
            bool validarNombreUsuarioUnico = _repo.IsUniqueUser(usuarioRegistroDto.NombreUsuario);

            if (!validarNombreUsuarioUnico){
                _respuestaApi.StatusCode = HttpStatusCode.BadRequest;
                _respuestaApi.IsSuccess = false;
                _respuestaApi.ErrorMessages.Add("El nombre de usuario ya existe");
                return BadRequest();
            }

            var usuario = await _repo.Registro(usuarioRegistroDto);

            if (usuario == null)
            {
                 _respuestaApi.StatusCode = HttpStatusCode.BadRequest;
                _respuestaApi.IsSuccess = false;
                _respuestaApi.ErrorMessages.Add("Error en el registro");
                return BadRequest();
            }

            _respuestaApi.StatusCode = HttpStatusCode.OK ;
            _respuestaApi.IsSuccess = true;
            
            return Ok(_respuestaApi);
        }
    
        [HttpPost("Login")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Login([FromBody] UsuarioLoginDto usuarioLoginDto)
        {
            var respuestaLogin  = await _repo.Login(usuarioLoginDto);


            if (respuestaLogin.usuario ==null || string.IsNullOrEmpty(respuestaLogin.token)){
                _respuestaApi.StatusCode = HttpStatusCode.BadRequest;
                _respuestaApi.IsSuccess = false;
                _respuestaApi.ErrorMessages.Add("Usuario o password incorrecto");
                return BadRequest();
            }

            _respuestaApi.StatusCode = HttpStatusCode.OK ;
            _respuestaApi.IsSuccess = true;
            _respuestaApi.result = respuestaLogin;
            
            return Ok(_respuestaApi);
        }
    }
}