using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using ApiPeliculas.Repositorio.IRepositorio;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ApiPeliculas.Controllers
{
    [ApiController]
    [Route("api/peliculas")]
    public class PelicuasController : ControllerBase
    {
        private readonly IPeliculaRepository  _peRepo;
        private readonly IMapper _iMapper;

        public PelicuasController(IPeliculaRepository  peliculaRepository, IMapper mapper)
        {
            this._peRepo = peliculaRepository;
            this._iMapper = mapper;   
        }



        [HttpGet]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult getPeliculas()
        {
            var ListePeliculas = _peRepo.GetPeliculas();
            var ListaPeliculasDto = new List<PeliculaDto>();


            foreach (var lista in ListePeliculas)
            {
                ListaPeliculasDto.Add(_iMapper.Map<PeliculaDto>(lista));
            }
            return Ok(ListaPeliculasDto);
        }

        [HttpGet(("peliculaId:int"), Name = "GetPelicula")]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult getCategoria(int peliculaId )
        {
            var itemPelicula = _peRepo.getPelicula(peliculaId);

            if (itemPelicula == null)
            {
                return NotFound();
            }

            var itemPeloculaDto = _iMapper.Map<PeliculaDto>(itemPelicula);
            return Ok(itemPeloculaDto);
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(PeliculaDto))]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult CrearPelicula([FromBody] PeliculaDto peliculaDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (peliculaDto == null)
            {
                return BadRequest(ModelState);
            }

            if (_peRepo.ExistePelicula(peliculaDto.Nombre))
            {
                ModelState.AddModelError("","La Pelicula ya existe");
                return StatusCode(404, ModelState);
            }

            var pelicula = _iMapper.Map<Pelicula>(peliculaDto);
            
            if (!_peRepo.CreatePelicula(pelicula))
            {
                ModelState.AddModelError("",$"Algo salió mal guardando el registro {pelicula.Nombre}");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetPelicula", new { peliculaId = pelicula.Id }, pelicula);
        }    

            
        [HttpPatch(("peliculaId:int"), Name = "ActualizarPatchPelicula")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult ActualizarPatchPelicula(int peliculaId, [FromBody] PeliculaDto peliculaDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (peliculaDto == null || peliculaId!=peliculaDto.Id)
            {
                return BadRequest(ModelState);
            }

            var pelicula = _iMapper.Map<Pelicula>(peliculaDto);
            if (!_peRepo.ActualizarPelicula(pelicula))
            {
                ModelState.AddModelError("",$"Algo salió mal actualizando el registro {pelicula.Nombre}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    
        [HttpDelete(("peliculaId:int"), Name = "BorrarPelicula")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult BorrarPelicula(int peliculaId)
        {

            if (!_peRepo.ExistePelicula(peliculaId))
            {
                return NotFound();
            }

            var pelicula = _peRepo.getPelicula(peliculaId);
            
            if (!_peRepo.BorrarPelicula(pelicula))
            {
                ModelState.AddModelError("",$"Algo salió mal borrano el registro {pelicula.Nombre}");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }


        [HttpGet("GetPeliculasEnCategoria/{categoriaId:int}")]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetPeliculasEnCategoria(int categoriaId)
        {
            var listaPeliculas = _peRepo.GetPeliculasEnCategoria(categoriaId);

            if (listaPeliculas== null){
                return NotFound();
            }

            var itemPelicula = new List<PeliculaDto>();

            foreach (var item in listaPeliculas)
            {
                itemPelicula.Add(_iMapper.Map<PeliculaDto>(item));
            }
            return Ok(itemPelicula);
        }        



        [HttpGet("Buscar")]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Buscar(string nombre)
        {

            try{

                var resultado = _peRepo.BuscarPelicula(nombre.Trim());

                if (resultado.Any()){
                    return Ok(resultado);
                }

                return NotFound();


            } catch (Exception er){
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error recuperando datos {er.Message}");
            }
        }        
    }
    
}