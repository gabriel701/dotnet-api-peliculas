using ApiPeliculas.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiPeliculas.Data;

public class ApplicationDbContext :DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
        
    }
    
    // agreta aqui los modelos
    public DbSet<Categoria> Categoria { set; get; }
    public DbSet<Pelicula> Pelicula { set; get; }
    public DbSet<Usuario> Usuario { set; get; }
}