using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiPeliculas.Models
{
    public class RespuestaApi
    {
        public RespuestaApi()
        {
            ErrorMessages = new List<string>();
        }

        public HttpStatusCode StatusCode {get; set;}
        public bool IsSuccess {get; set;} = true;

        public List<string> ErrorMessages {get; set;}

        public object result {get; set;}  
    }
}