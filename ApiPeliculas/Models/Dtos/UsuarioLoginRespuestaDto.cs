using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Models.Dtos
{
    public class UsuarioLoginRespuestaDto
    {
        public Usuario usuario {get; set;}
        public string token{ get; set; }
    }
}