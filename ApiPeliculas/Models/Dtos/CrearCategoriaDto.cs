using System.ComponentModel.DataAnnotations;

namespace ApiPeliculas.Models;

public class CrearCategoriaDto
{
    [Required(ErrorMessage = "El nombre es obligatorio")]
    [MaxLength(100, ErrorMessage = "El número maximo de caracteres es 100!")]
    public string Nombre { set; get; }
}