using System.ComponentModel.DataAnnotations;

namespace ApiPeliculas.Models;

public class CategoriaDto
{
    
    public int Id { get; set; }

    [Required(ErrorMessage = "El nombre es obligatorio")]
    [MaxLength(60, ErrorMessage = "El número maximo de caracteres es 60!")]
    public string Nombre { set; get; }
    
}