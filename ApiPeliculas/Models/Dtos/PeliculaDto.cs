using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Models.Dtos
{
    public class PeliculaDto
    {
            public enum TipoClasificacion{ Siete, Trece, Diecises, Dieciocho }
    
    
    public int Id { get; set; }
    [Required(ErrorMessage = "El nombre es obligatorio")]
    public string Nombre { get; set; }
    public string RutaImagen { get; set ; }
    [Required(ErrorMessage = "La descripción es obligatorio")]
    public string  Descripcion { get; set; }
    [Required(ErrorMessage = "La duración es obligatorio")]
    public int Duracion { get; set; }
    public TipoClasificacion Clasificacion { set; get; }
    public DateTime FechaCreacion { get; set; }

    public int CategoriaId { get; set; }
    }
}