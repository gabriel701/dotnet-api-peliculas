using System.ComponentModel.DataAnnotations;

namespace ApiPeliculas.Models;

public class Categoria
{
    [Key]
    public int Id { get; set; }

    [Required()] 
    public string Nombre { set; get; }

    public DateTime FechaCreacion { set; get; }
}