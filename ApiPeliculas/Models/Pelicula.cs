using System.ComponentModel.DataAnnotations.Schema;

namespace ApiPeliculas.Models;

public class Pelicula
{
    public enum TipoClasificacion{ Siete, Trece, Diecises, Dieciocho }
    
    
    public int Id { get; set; }
    public string Nombre { get; set; }
    public string RutaImagen { get; set ; }
    public string  Descripcion { get; set; }
    public int Duracion { get; set; }
    public TipoClasificacion Clasificacion { set; get; }
    public DateTime FechaCreacion { get; set; }
    [ForeignKey("CategoriaId")]
    public int CategoriaId { get; set; }
    public Categoria Categoria { get; set; }
}