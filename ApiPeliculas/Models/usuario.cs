using System.ComponentModel.DataAnnotations;

namespace ApiPeliculas.Models;

public class Usuario
{
    [Key]
    public int Id { get; set; }
    public string NombreUsuario { set; get; }
    public string Nombre { get; set; }
    public string Password { get; set; }
    public string Role { get; set; }
}