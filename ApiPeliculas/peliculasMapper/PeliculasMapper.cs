using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ApiPeliculas.PeliculasMapper;

public class PeliculasMapper : Profile
{
    public PeliculasMapper()
    {
        CreateMap<Categoria, CategoriaDto>().ReverseMap();
        CreateMap<Categoria, CrearCategoriaDto>().ReverseMap();
        CreateMap<Pelicula, PeliculaDto>().ReverseMap();
        CreateMap<Usuario, UsuarioLoginDto>().ReverseMap();
        CreateMap<Usuario, UsuarioDto>().ReverseMap();
        CreateMap<Usuario, UsuarioRegistroDto>().ReverseMap();
        CreateMap<Usuario, UsuarioLoginRespuestaDto>().ReverseMap();
    }
}