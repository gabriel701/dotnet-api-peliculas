using ApiPeliculas.Data;
using ApiPeliculas.PeliculasMapper;
using ApiPeliculas.Repositorio;
using ApiPeliculas.Repositorio.IRepositorio;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

// configurar cadena de conexion 

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    var versionServidor = ServerVersion.Parse("8.0.28-mysql");

    options.UseMySql(builder.Configuration.GetConnectionString("DefaultConnection"),
        versionServidor);
});

//agregamos los repositorios
builder.Services.AddScoped<ICategoriaRepository, CategoriaRepositorio>();
builder.Services.AddScoped<IPeliculaRepository, PeliculaRepositorio>();
builder.Services.AddScoped<IUsuarioRepository, UsuarioRepositorio>();

// agregamos el automaper  
builder.Services.AddAutoMapper(typeof(PeliculasMapper));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// soporte para CORS
// MUY IMPORTANTE
// se deja abierto para todos los dominios, ajustar en el caso necesario
builder.Services.AddCors(p => p.AddPolicy("PolicyCors", build => {
    build.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
})) ;

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

// soporte para cors
app.UseCors("PolicyCors");

app.UseAuthorization();

app.MapControllers();

app.Run();
